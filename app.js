const Koa = require("koa");
const app = new Koa();
const eagle = require("node-eagle");
const config = require("./config");

// for assets folder
const koastatic = require('koa-static');
const path = require('path');
const resfolder = path.resolve(__dirname, './assets');
app.use((ctx, next) => {
    ctx.path = ctx.path.replace('/assets/', '/');
    return next();
});
app.use(koastatic(resfolder, {
    maxage: 60 * 60 * 1000
}));
// end assets

eagle(app, config);

app.listen(config.port, config.host, function () {
    console.log(`app start at ${config.host}:${config.port}`);

    require('./assets/build');
});
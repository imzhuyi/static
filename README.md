## 开发环境准备：
1. 安装 node8+
2. 运行 npm i node-sass --global
3. 运行 npm i nodemon --global
4. 进入项目根目录运行 npm i
5. 启动项目（自动监听改动） npm start

## 其它命令：
* 编译静态资源：npm run fe
* 指定编译某些目录：npm run fe -- aa,bb
* 生成html文件：在启动项目后运行 node save.js

const request = require('request');
const fs = require('fs');
const config = require('./config');

const pages = [
    'index',
    'project',
    'finance',
    'invest',
    'sales',
];
const regHref = /\shref="(\/[a-z0-9\-_]+){1,3}(?=")/ig;

pages.forEach(function (page) {
    (function (page) {
        request({
            url: `http://${config.host}:${config.port}/${page}`,
            method: 'GET'
        }, function (err, res, body) {
            body = body.replace(regHref, function(a,b){
                return a + ".html";
            });
            fs.writeFileSync(`${__dirname}/html/${page}.html`, body, {
                encoding: 'utf8'
            });
            console.log(page, '--saved');
        });
    }(page));
});